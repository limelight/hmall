package com.heima.demo;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class User {
    public String name;
    public int age;

//    @Autowired
//    private  Phone phone;

    public User(String name, int age) {
        System.out.println("User Construct Method");
        this.name = name;
        this.age = age;
    }


    @PostConstruct
    public void foo() {
        System.out.println("finally, user instance is created");
    }

    public void bar() {

    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
