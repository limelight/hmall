package com.heima.demo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DemoApplication {


    @Bean
    public User getUser() {
        return new User("rusty", 19);
    }


    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoApplication.class);
        User user = context.getBean(User.class);
        System.out.println(user);

    }


}
