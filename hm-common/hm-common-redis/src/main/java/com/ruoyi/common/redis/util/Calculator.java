package com.ruoyi.common.redis.util;

public class Calculator {
    public static int add(int a, int b) {
        return a + b;
    }

    public static int minus(int a, int b) {
        return a - b;
    }
}
