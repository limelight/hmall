package com.hmall.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.hmall.user.mapper")
@SpringBootApplication
public class UserApplication {

//    @Value("${spring.profiles.active}")
//    private String env;

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
//        System.out.println("env:" + env);
    }
}
