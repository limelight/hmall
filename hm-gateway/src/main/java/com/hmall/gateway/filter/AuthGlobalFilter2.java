package com.hmall.gateway.filter;

import com.hmall.gateway.config.AuthProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 1. 创建FilteringWebHandler对象需要
 * private final List<GatewayFilter> globalFilters;
 * <p>
 * public FilteringWebHandler(List<GlobalFilter> globalFilters) {
 * this.globalFilters = loadFilters(globalFilters);
 * }
 * 2.
 * GlobalFilter注入的核心代码位于
 * <p>
 * DefaultListableBeanFactory#resolveMultipleBeans方法
 * 其中beanName = filteringWebHandler
 * elementType = interface org.springframework.cloud.gateway.filter.GlobalFilter
 * descriptor = method 'filteringWebHandler' parameter 0
 * Map<String, Object> matchingBeans = findAutowireCandidates(beanName, elementType,
 * new MultiElementDescriptor(descriptor));
 * <p>
 * 即可通过从容器中寻找interface org.springframework.cloud.gateway.filter.GlobalFilter实现类实现
 * <p>
 * <p>
 * <p>
 * Filter的order值越小，就越先执行
 */
@Component
@EnableConfigurationProperties(AuthProperties.class)
@Slf4j
public class AuthGlobalFilter2 implements GlobalFilter, Ordered {

    public AuthGlobalFilter2() {
        int i = 0;
    }

    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        return chain.filter(exchange);
    }
}