package com.hmall.gateway.route;

import cn.hutool.json.JSONUtil;
import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import com.ruoyi.common.core.utils.CollUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionWriter;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;

/**
 * 网关配置变化监听器
 * 网关无法直接通过nacos进行更新，因为通过两步手动实现
 * 1. 连接nacos服务器监听变化；
 * 2. 当路由表发生变化时，使用代码更新路由。
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class RouterHotUpdateLoader {
    // 路由配置文件的id和分组
    private final String DATA_ID = "gateway-routes.json";
    private final String Group = "DEFAULT_GROUP";

    private final RouteDefinitionWriter writer;
    private final NacosConfigManager nacosConfigManager;


    // 保存更新过的路由id
    private final Set<String> routeIds = new HashSet<>();

    @PostConstruct
    public void initRouteConfigChangedListener() throws NacosException {
        // 1.注册监听器并首次拉取配置
        String configInfo = nacosConfigManager.getConfigService()
                .getConfigAndSignListener(DATA_ID, Group, 5000, new Listener() {
                    @Override
                    public Executor getExecutor() {
                        return null;
                    }

                    @Override
                    public void receiveConfigInfo(String configInfo) {
                        onRemoteConfigChanged(configInfo);
                    }
                });
        // 2.首次启动时，更新一次配置
        onRemoteConfigChanged(configInfo);
    }

    /**
     * 当指定的Data Id文件的内容发生改变时，获取其最新的配置内容
     *
     * @param data
     */
    private void onRemoteConfigChanged(String data) {
        log.debug("监听到路由配置变更，{}", data);
        // 1.反序列化
        List<RouteDefinition> routeDefinitions = JSONUtil.toList(data, RouteDefinition.class);
        // 2.更新前先清空旧路由
        // 2.1.清除旧路由
        for (String routeId : routeIds) {
            writer.delete(Mono.just(routeId)).subscribe();
        }
        routeIds.clear();
        // 2.2.判断是否有新的路由要更新
        if (CollUtils.isEmpty(routeDefinitions)) {
            // 无新路由配置，直接结束
            return;
        }
        // 3.更新路由
        routeDefinitions.forEach(routeDefinition -> {
            // 3.1.更新路由
            writer.save(Mono.just(routeDefinition)).subscribe();
            // 3.2.记录路由id，方便将来删除
            routeIds.add(routeDefinition.getId());
        });
    }
}
